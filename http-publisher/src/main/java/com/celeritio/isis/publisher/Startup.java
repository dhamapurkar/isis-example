package com.celeritio.isis.publisher;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.time.StopWatch;
import org.apache.log4j.Logger;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpClient;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.WebClient;



public class Startup 
{
	/**
	 * Long wait time
	 */
	private static final long SLEEP_TIME = 50000; 
	
	private static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
	
	private static HttpClient client;

	private static WebClient webClient;
	
	private static final String URI = "/restful/services/simple.SimpleObjectMenu/actions/create/invoke";

	private static final String WEBURI = "http://localhost:8080/restful/services/simple.SimpleObjectMenu/actions/create/invoke";
	
	public static final String CONTENT_TYPE  ="Content-Type";
	
	public static final String CONTENT_TYPE_JSON ="application/json";
	
	public static Integer requestsSent = 0;
	
	private static final Logger log  = Logger.getLogger(Startup.class);
	
	public static void main(String[] args) throws Exception 
	{
		RestPublisher publisher = new RestPublisher();
		
		// 1. with httpClient
		client = publisher.createAndGetHttpClient();
		scheduler.scheduleAtFixedRate(() -> crateSimpleObjects(), 500, 500, TimeUnit.MILLISECONDS);
		
		
		// 2. with webClient of vertx 
		//scheduler.scheduleAtFixedRate(() -> crateSimpleObjectsWithWebClient(), 500, 500, TimeUnit.MILLISECONDS);
		//webClient =  publisher.createAndGetWebclient();
		
		try 
		{
			/**
			 * Scheduler will keep publishing objects till we go out of sleep
			 */
			Thread.sleep(SLEEP_TIME);
		}
		
		catch (InterruptedException e) 
		{
			log.info("Thread interrupted",e);
		}
	}
	
	
	public static void crateSimpleObjects() 
	{	
		long currentTimeMillis = System.currentTimeMillis();
		
		String timeAsString  = String.valueOf(currentTimeMillis);
		SimpleObjectJ jsonObject = new SimpleObjectJ();
		jsonObject.setName(timeAsString);
		
		StopWatch watch = StopWatch.createStarted();
		
		client.post(URI, httpcliResp ->
		{
			watch.stop();
			httpcliResp.exceptionHandler(x -> {
				log.info("Failed for " + timeAsString);
			});
			httpcliResp.bodyHandler(buff -> {
				log.info(" Object Response Time = " + watch.getTime(TimeUnit.MILLISECONDS) + " (ms) with Status code =" + httpcliResp.statusCode());
		});

		}).putHeader("cache-control", "no-cache")
		        .putHeader("Authorization", "Basic c3ZlbjpwYXNz")
		        .putHeader(CONTENT_TYPE, CONTENT_TYPE_JSON).setChunked(true)
		        .write(jsonObject.toBuffer()).end();
		requestsSent++;
		log.info("Requests Sent Counter =" +requestsSent);
		
		
	}
	
	/**
	 * Uses a web client  which is a wrapper over httpclient
	 */
	public static void crateSimpleObjectsWithWebClient() 
	{	
		try{ 
			long currentTimeMillis = System.currentTimeMillis();
		
		String timeAsString  = String.valueOf(currentTimeMillis);
		SimpleObjectJ jsonObject = new SimpleObjectJ();
		jsonObject.setName(timeAsString);
		
		StopWatch watch = StopWatch.createStarted();
		
		HttpRequest<Buffer> httpRequest = webClient.postAbs(WEBURI);

		httpRequest.putHeader("cache-control", "no-cache");
		httpRequest.putHeader("Authorization", "Basic c3ZlbjpwYXNz");
		httpRequest.putHeader(CONTENT_TYPE, CONTENT_TYPE_JSON);
		
		httpRequest.sendJsonObject(jsonObject, handler ->{
			
			// Handler called stop the watch
			watch.stop();
			log.info("Total Time in webClient =" + watch.getTime(TimeUnit.MILLISECONDS) );
			
		});

		requestsSent++;
		log.info("Requests Sent Counter =" +requestsSent);
		
		} catch(Exception e) {
			log.info(" exception in webClient",e);
		}
		
	}
}
