package com.celeritio.isis.publisher;

import org.apache.log4j.Logger;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.eventbus.EventBusOptions;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;

public class RestPublisher extends AbstractVerticle
{

	private static final int VERTX_PORT = 6567;
	private static final int ISIS_PORT = 8080;
	private static final String LOCALHOST = "localhost";
	private static final Logger log = Logger.getLogger(RestPublisher.class);
	
	
	public RestPublisher() throws Exception
	{
		super();
		init();
	}
	public void init() throws Exception
	{

		EventBusOptions ebo = new EventBusOptions();
		ebo.setClustered(false);
		ebo.setPort(VERTX_PORT);
		ebo.setHost(LOCALHOST);

		VertxOptions vertxoption = new VertxOptions();
		vertxoption.setEventBusOptions(ebo);
		
	
		Vertx vertx = Vertx.vertx(vertxoption);
		vertx.deployVerticle(this,new DeploymentOptions().setWorker(true));
		log.info("[Deployed vertx Verticle successfully.]");
	}
	
	public HttpClient createAndGetHttpClient()
	{	
		HttpClientOptions options = 	new HttpClientOptions().setKeepAlive(true).setDefaultHost(LOCALHOST)
        .setDefaultPort(ISIS_PORT).setPipelining(true).setPipeliningLimit(20).setSsl(false);
		return getVertx().createHttpClient(options);
	}
	
	
	public WebClient createAndGetWebclient() {
		
		WebClientOptions webClientOptions = new WebClientOptions();
		webClientOptions.setDefaultHost(LOCALHOST);
		webClientOptions.setConnectTimeout(500);
		webClientOptions.setDefaultPort(ISIS_PORT);
		
		return WebClient.create(getVertx(),webClientOptions);
	}
}
