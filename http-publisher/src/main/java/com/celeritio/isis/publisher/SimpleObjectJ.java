package com.celeritio.isis.publisher;

import io.vertx.core.json.JsonObject;

public class SimpleObjectJ  extends JsonObject{
	
	private static final String VALUE = "value";
	
	private String name = "name";
	
	public void setName(String name) 
	{
		JsonObject jName = new JsonObject ();
		jName.put(VALUE,  name );
		put(this.name, jName);
	}
	 
}
